#pragma once
class Matrice
{
	// Parametres
	int sizeMat;
	int** matrice;

public:
	// Constructeur
	Matrice(int size);
	// Destructeur
	~Matrice();
	// Surcharge d�op�rateurs
	Matrice* operator*(const Matrice &matriceB);
	Matrice* operator+(const Matrice &matriceB);
	Matrice* operator-(const Matrice &matriceB);
	// Rend la matrice en puissance de 2 (Utile pour Strassen)
	Matrice* power2Matrice();
	// D�coupe une matrice en 4 sous-matrice 
	void splitMatrice(Matrice * matriceUL, Matrice * matriceUR, Matrice * matriceDL, Matrice * matriceDR);
	// Reforme la matrice d�coup�e en 4
	void rebuildMatrice(Matrice * matriceUL, Matrice * matriceUR, Matrice * matriceDL, Matrice * matriceDR);
	// Remplissage d�une matrice
	void fillZero();
	void fillRand();
	// Affichage
	void printMatrice();
	// Getters
	int getSizeMat() const;
	int** getMatrice() const;
	// Multiplication naive
	Matrice * classicMultiply(Matrice * matriceA, Matrice * matriceB);
};

