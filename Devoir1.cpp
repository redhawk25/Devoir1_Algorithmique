/********************************************
#       Devoir 1 - Algorithmique            #
#    Nicolas FARINE - UQAC - Et� 2016       #
#                                           #
#   Main du programme, s�lection du mode    #
*********************************************/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <istream>
#include <stdbool.h>
#include <algorithm>
#include "Matrice.hpp"

using namespace std;
// N�cessaire pour corriger l�erreur "cout est ambigue"
//using std::cout;

// ## D�claration des fonctions ##
// S�l�ctions
void selectSortAlgorithm();
// Strassen
Matrice* strassen(Matrice* matriceA, Matrice* matriceB);
// Tris
void swapValues(int& first, int& second);
int partition(int tab[], int left, int right, bool isRand);
void triFusion(int tab[], int sizeTab);
void fusion(int tabLeft[], int tabRight[], int sizeTab);
bool isEmpty(int tab[], int sizeTab);
void triBulles(int tab[], int sizeTab);
void quickSort(int tab[], int begin, int end, bool isRand);
void quickSortRand(int tab[], int begin, int end, bool isRand);
void triInsertion(int tab[], int sizeTab);
void triTas(int tab[], int sizeTab);
void tasser(int tab[], int sizeTab, int node);
void triBase();
void printTab(int tab[], int sizeTab);
void arrayGenerator(int tab[], int n);
void copyTab(int tabOrigin[], int tab[], int sizeTab);


/*  #################
	##  Interface  ##
	#################  */
int main()
{
	int choice = 10;
	Matrice* matriceA = new Matrice(8);
	matriceA->fillRand();
	Matrice* matriceB = new Matrice(8);
	matriceB->fillRand();

	while (choice != 0)
	{
		cout << "###################################### " << endl;
		cout << "#      Devoir 1 - Algorithmique      # " << endl;
		cout << "#  Nicolas FARINE - UQAC - ETE 2016  # " << endl;
		cout << "###################################### " << endl;
		cout << endl;
		cout << "1 - Multiplication matricielle : Algorithme de Strassen" << endl;
		cout << "2 - Algorithmes de tri" << endl;
		cout << "3 - Grand nombre : Algorithme de Karatsuba" << endl;
		cout << "0 - Quitter" << endl;
		cout << endl << "Choix : ";
		cin >> choice;

		switch (choice)
		{
		case 1:
			// Affichage des 2 matrices
			cout << endl << "-------------------------" <<endl;
			cout << "Matrice A :";
			matriceA->printMatrice();
			cout << "Matrice B :";
			matriceB->printMatrice();
			cout << "-------------------------" << endl << endl;
			// Affichage des r�sultats
			cout << "** Algorithme naif **" << endl;
			matriceA->classicMultiply(matriceA, matriceB)->printMatrice();
			cout << "** Algorithme de Strassen **" << endl;
			strassen(matriceA, matriceB)->printMatrice();
			break;
		case 2:
			// S�lection du tri
			selectSortAlgorithm();
			cout << "En cours" << endl;
			break;
		case 3:
			cout << "En cours" << endl;
			break;
		case 0:
			break;
		default:
			cout << endl << "** Choix non disponible **" << endl;
			break;
		}
		cout << endl << endl;
	}
	//cout << endl;
	//cout << "Press anything + Enter to end Program" << endl;
	//cin >> choice;

	return 0;
}

void selectSortAlgorithm()
{
	// Valeurs du tableau
	int sizeTab = 0;

	cout << endl << "** Algorithmes de tri **" << endl;
	cout << "Taille du tableau ?" << endl;
	cin >> sizeTab;
	int *tabOrigin = new int[sizeTab];
	int *tab = new int[sizeTab];
	int *tab2 = new int[sizeTab-1];
	arrayGenerator(tabOrigin, sizeTab);
	copyTab(tabOrigin, tab, sizeTab);
	cout << endl << "Tableau :";
	printTab(tabOrigin, sizeTab);

	int choice = 10;
	while (choice != 0)
	{
		cout << "** Algorithmes de tri **" << endl;
		cout << "1 - Tri par fusion" << endl;
		cout << "2 - Tri par bulles" << endl;
		cout << "3 - QuickSort" << endl;
		cout << "4 - QuickSort rand" << endl;
		cout << "5 - Tri par insertion" << endl;
		cout << "6 - Tri par tas" << endl;
		cout << "7 - Tri par base" << endl;
		cout << "8 - STL : sort" << endl;
		cout << "9 - STL : sort_heap" << endl;
		cout << "10 - STL : stable_sort" << endl;
		cout << "0 - Menu principal" << endl;
		cout << endl << "Choix : ";
		cin >> choice;

		switch (choice)
		{
		case 1:
			cout << endl << "---- Tri fusion ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			triFusion(tab, sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << endl << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 2:
			cout << endl << "---- Tri par bulles ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			triBulles(tab, sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << endl << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab-1);
			printTab(tab, sizeTab);
			sizeTab = sizeTab / 2;
			break;
		case 3:
			cout << endl << "---- QuickSort ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			quickSort(tab, 0, sizeTab-1, false);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << endl << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 4:
			cout << endl << "---- QuickSort Random ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			quickSortRand(tab, 0, sizeTab - 1, true);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << endl << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 5:
			cout << endl << "---- Tri par insertion ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			triInsertion(tab, sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 6:
			cout << endl << "---- Tri par tas ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			triTas(tab, sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 7:
			//triBase();
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 8:
			cout << endl << "---- STL : sort ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			sort(tab, tab + sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 9:
			cout << endl << "---- STL : sort_heap ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			make_heap(tab, tab + sizeTab);
			sort_heap(tab, tab + sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		case 10:
			cout << endl << "---- STL : stable_sort ----" << endl << endl;
			cout << "Tableau avant tri : ";
			printTab(tab, sizeTab);
			stable_sort(tab, tab + sizeTab);
			cout << "Tableau apres tri : ";
			printTab(tab, sizeTab);
			cout << "------------------------" << endl;
			copyTab(tabOrigin, tab, sizeTab);
			break;
		default:
			cout << endl << "** Choix non disponible **" << endl;
			break;
		}
	cout << endl;
	}
	delete[] tab;
	delete[] tabOrigin;
}

/*  ###############################
	##  Algorithmes de Strassen  ##
	###############################  */

// Strassen
Matrice* strassen(Matrice* matriceA, Matrice* matriceB)
{
	// On calibre les deux matrices pour que leur taille soit une puissance de 2
	matriceA->power2Matrice();
	matriceB->power2Matrice();
	// Cr�ation dela matrice de r�sultat
	Matrice* strassen = new Matrice(matriceA->getSizeMat());
	// Calcul du Strassen par la surchage de l�op�rateur *
	strassen = *matriceA * *matriceB;
	return strassen;
}


/*  ##########################
	##  Algorithmes de tri  ##
	##########################  */

// Fonction d�inversion de 2 valeurs d�un tableau
void swapValues(int& first, int& second)
{
	int tmp = first;
	first = second;
	second = tmp;
}

int partition(int tab[], int left, int right, bool isRand)
{
	int pivot;
	// QuickSort avec pivot al�atoire
	if (isRand)
	{
		pivot = tab[rand() % left + right];
	}
	// QuickSort avec pivot fixe (dernier �l�ment)
	else
	{
		pivot = tab[right];
	}

	int i = left;
	for (int j = left; j <= right - 1;j++)
	{
		if (tab[j] <= pivot)
		{
			swapValues(tab[i], tab[j]);
			i++;
		}
	}
	swapValues(tab[i], tab[right]);
	return i;
}

// Tri fusion
void triFusion(int tab[], int sizeTab)
{
	// Si le tableau est de 1 ou 0 �l�ment, il est d�j� tri�
	if (sizeTab <= 1)
	{
		return;
	}
	// Cr�ation des sous-tableaux
	int sizeTabLeft = sizeTab / 2;
	int sizeTabRight = sizeTab - sizeTabLeft;
	int *tabLeft = new int[sizeTabLeft];
	int *tabRight = new int[sizeTabRight];
	int left = 0;
	int right = 0;

	for (int i = 0;i < sizeTab;i++)
	{
		if (i % 2 == 0)
		{
			tabRight[right] = tab[i];
			right++;
		}
		else
		{
			tabLeft[left] = tab[i];
			left++;
		}
	}

	// Appel r�cursif du tri
	triFusion(tabLeft, sizeTabLeft);
	triFusion(tabRight, sizeTabRight);

	fusion(tabLeft, tabRight, sizeTab);
}

void fusion(int tabLeft[], int tabRight[], int sizeTab)
{
	int sizeSubTab = sizeTab/2;
	int *tabFusion = new int[sizeTab];

	while (!isEmpty(tabLeft, sizeSubTab) && !isEmpty(tabRight, sizeSubTab))
	{
		if (tabLeft[0] <= tabRight[0])
		{
			
		}
	}
}

bool isEmpty(int tab[], int sizeTab)
{
	if (sizeTab == 0)
	{
		return true;
	}
	else return false;
}

// Tri par bulles
void triBulles(int tab[], int sizeTab)
{
	// Si le tableau est de 1 ou 0 �l�ment, il est d�j� tri�
	if (sizeTab <= 1)
	{
		return;
	}
	// Vrai si le tableau est tri� correctement
	bool tabSorted = false;
	// Sortie uniquement lorsque le tableau est tri�
	while (!tabSorted)
	{
		// On consid�re que le tableau est tri� correctement
		// Si toutes les valeurs sont correctement tri�es, on entre pas dans la boucle if, la valeur reste � vrai, on sort de la boucle while
		tabSorted = true;
		// Parcours de tous les �l�ments
		for (int i = 0; i < sizeTab-1; i++)
		{
			// Si les deux valeurs adjacentes ne sont pas tri�s correctement
			if (tab[i] > tab[i + 1])
			{
				// On intervertie les valeurs
				swapValues(tab[i], tab[i + 1]);
				// On consid�re que le tableau n�est pas encore tri� correctement
				tabSorted = false;
			}
		}
	}
}

// QuickSort
void quickSort(int tab[], int left, int right, bool isRand)
{
	// Inutile de v�rifier si la taille est <=1 puisque "left<right" est fausse dans ce cas l�
	if (left < right)
	{
		int part = partition(tab, left, right, isRand);
		quickSort(tab, left, part-1, isRand);
		quickSort(tab, part+1, right, isRand);
	}

}

// QuickSort rand
void quickSortRand(int tab[], int left, int right, bool isRand)
{
	// V�rification de la taille inutile (cf QuickSort)
	quickSort(tab, left, right, false);
}

// Tri par insertion
void triInsertion(int tab[], int sizeTab)
{
	// Si le tableau est de 1 ou 0 �l�ment, il est d�j� tri�
	if (sizeTab <= 1)
	{
		return;
	}
	// Parcours du tableau
	for (int i = 0; i < sizeTab;i++)
	{
		// Sauvegarde de la valeur de tab[i]
		int tmp = tab[i];
		// Point de d�part du parcours
		int j = i;
		// Tant que la valeur pr�c�dente est inf�rieure a notre valeur consid�r�e
		while (j > 0 && tab[j - 1] > tmp)
		{
			tab[j] = tab[j - 1];
			j--;
		}
		tab[j] = tmp;
	}
}

// Tri par tas
void triTas(int tab[], int sizeTab)
{
	// Si le tableau est de 1 ou 0 �l�ment, il est d�j� tri�
	if (sizeTab <= 1)
	{
		return;
	}
	// R�arangement initial
	for (int i = ((sizeTab / 2) - 1); i >= 0; i--)
		tasser(tab, sizeTab, i);

	// On trie au fur et � mesure
	for (int i = sizeTab - 1; i >= 0; i--)
	{
		// On inverse la valeur courante et la derni�re valeur
		swapValues(tab[0], tab[i]);

		// Appel r�cursif sur le tableau r�duit (valeur encore non tri�e)
		tasser(tab, i, 0);
	}
}

void tasser(int tab[], int sizeTab, int node)
{
	// Le plus grand devient la racine
	int highest = node;
	// Fils gauche
	int fGauche = ((2 * highest) + 1);
	// Fils droit
	int fDroite = ((2 * highest) + 2);

	// Si le fils gauche est plus grand que la racine, il devient racine
	if (fGauche < node && tab[fGauche] > tab[highest])
	{
		highest = fGauche;
	}
	// Si le fils droit est plus grand que la racine, il devient racine
	if (fDroite < node && tab[fDroite] > tab[highest])
	{
		highest = fDroite;
	}
	// Si le plus grand n�est pas la racine, on �change les valeurs avec le noeud courant
	// et on appelle r�cursivement la fonction tasser
	if (highest != node)
	{
		swapValues(tab[node], tab[highest]);
		tasser(tab, sizeTab, highest);
	}
}

// Tri par base
void triBase()
{

}

// Affichage du tableau
void printTab(int tab[], int sizeTab)
{
	cout << endl << "[ ";
	int i;
	for (i = 0;i < sizeTab-1;i++)
	{
		cout << tab[i] << ", ";
	}
	cout << tab[i] <<" ]" << endl << endl;
}

// G�n�ration d�un tableau contenant n �l�ments al�atoirement g�n�r�s entre 0 et INT_MAX(limits)
void arrayGenerator(int tab[], int n)
{
	for (int i = 0;i < n;i++)
	{
		tab[i] = rand() % INT_MAX;
	}
}

void copyTab(int tabOrigin[], int tab[], int sizeTab)
{
	for (int i = 0;i < sizeTab;i++)
	{
		tab[i] = tabOrigin[i];
	}
}
