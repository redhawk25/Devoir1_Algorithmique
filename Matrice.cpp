#include "stdafx.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <istream>
#include "Matrice.hpp"
#include <limits>
using namespace std;

Matrice::Matrice(int size)
{
	matrice = (int**)malloc(size * sizeof(int*));
	for (int i = 0; i < size; i++)
	{
		matrice[i] = (int*)malloc(size * sizeof(int));
	}
	/*
	matrice = (int**)malloc(size * sizeof(int*));
	int* matrice2 = (int*)malloc(size * size * sizeof(int));
	for (int i = 0; i < size; i++)
	{
		matrice[i] = &matrice2[i*size];
	}*/
	// Taille de la matrice carr�
	sizeMat = size;
}

Matrice::~Matrice()
{
	// On lib�re d�abord chaque champ de la matrice
	for (int i = sizeMat-1; i >= 0; i--)
	{
		//for (int j = sizeMat - 1; j >= 0; j--)
		//{
		delete[] matrice[i];
		matrice[i] = NULL;
		//}
		//free(matrice[i]);
	}
	// Puis on lib�re la matrice
	delete[] matrice;
	matrice = NULL;
	// Et l�attribut taille
	sizeMat = NULL;
}

Matrice *Matrice::operator*(const Matrice &matriceB)
{
	// On cr�� la matrice de r�sultat
	Matrice* result = new Matrice(sizeMat);
	Matrice * newMatriceB = new Matrice(matriceB);


	// V�rification de la dimension (on ne travaille qu�avec des matrices carr�s)
	if (sizeMat != matriceB.sizeMat)
	{
		cout << "Les matrices n'ont pas la meme dimension !" << endl << endl;
	}
	else if (sizeMat <= 2)
	{
		return this->classicMultiply(this, newMatriceB);
	}
	else
	{
		// On divise la matrice A en 4 sous-matrices
		Matrice* strassenUL_A = new Matrice((sizeMat) / 2);
		Matrice* strassenUR_A = new Matrice((sizeMat) / 2);
		Matrice* strassenDL_A = new Matrice((sizeMat) / 2);
		Matrice* strassenDR_A = new Matrice((sizeMat) / 2);

		this->splitMatrice(strassenUL_A, strassenUR_A, strassenDL_A, strassenDR_A);

		// On divise la matrice B en 4 sous-matrices
		Matrice* strassenUL_B = new Matrice((matriceB.sizeMat) / 2);
		Matrice* strassenUR_B = new Matrice((matriceB.sizeMat) / 2);
		Matrice* strassenDL_B = new Matrice((matriceB.sizeMat) / 2);
		Matrice* strassenDR_B = new Matrice((matriceB.sizeMat) / 2);

		newMatriceB->splitMatrice(strassenUL_B, strassenUR_B, strassenDL_B, strassenDR_B);
		
		// On cr�� les 7 nouvelles matrices de Strassen
		Matrice* m1 = (*(*strassenUL_A + *strassenDR_A)) * (*(*strassenUL_B + *strassenDR_B));
		Matrice* m2 = (*(*strassenDL_A + *strassenDR_A)) * (*strassenUL_B);
		Matrice* m3 = (*strassenUL_A) * (*(*strassenUR_B - *strassenDR_B));
		Matrice* m4 = (*strassenDR_A) * (*(*strassenDL_B - *strassenUL_B));
		Matrice* m5 = (*(*strassenUL_A + *strassenUR_A)) * (*strassenDR_B);
		Matrice* m6 = (*(*strassenDL_A - *strassenUL_A)) * (*(*strassenUL_B + *strassenUR_B));
		Matrice* m7 = (*(*strassenUR_A - *strassenDR_A)) * (*(*strassenDL_B + *strassenDR_B));

		// On divise la matrice de r�sultat
		Matrice* resultUL = new Matrice((matriceB.sizeMat) / 2);
		Matrice* resultUR = new Matrice((matriceB.sizeMat) / 2);
		Matrice* resultDL = new Matrice((matriceB.sizeMat) / 2);
		Matrice* resultDR = new Matrice((matriceB.sizeMat) / 2);

		// On effectue les calculs n�cessaires pour obtenir le bon r�sultat
		resultUL = (*(*(*m1 + *m4) - *m5) + *m7);
		resultUR = *m3 + *m5;
		resultDL = *m2 + *m4;
		resultDR = *(*m1 - *m2) + *(*m3+ *m6);
		
		//On reconstruit la matrice de r�sultat
		result->rebuildMatrice(resultUL, resultUR, resultDL, resultDR);
		
		// On lib�re la m�moire (Non fonctionnel)
		delete m1;
		delete m2;
		delete m3;
		delete m4;
		delete m5;
		delete m6;
		delete m7;
		delete resultUL;
		delete resultUR;
		delete resultDL;
		delete resultDR;
		delete strassenUL_A;
		delete strassenUR_A;
		delete strassenDL_A;
		delete strassenDR_A;
		delete strassenUL_B;
		delete strassenUR_B;
		delete strassenDL_B;
		delete strassenDR_B;

		return result;
	}
}

Matrice *Matrice::operator+(const Matrice &matriceB)
{
	// V�rification de la dimension (on ne travaille qu�avec des matrices carr�s)
	if (sizeMat != matriceB.sizeMat)
	{
		cout << "Les matrices n'ont pas la meme dimension !" << endl << endl;
	}
	Matrice* addition = new Matrice(sizeMat);
	addition->fillZero();
	for (unsigned int i = 0; i < sizeMat; ++i) {
		for (unsigned int j = 0; j < sizeMat; ++j) {
			addition->getMatrice()[i][j] = matrice[i][j] + matriceB.matrice[i][j];
		}
	}
	return addition;
}

Matrice *Matrice::operator-(const Matrice &matriceB)
{
	// V�rification de la dimension (on ne travaille qu�avec des matrices carr�s)
	if (sizeMat != matriceB.sizeMat)
	{
		cout << "Les matrices n'ont pas la meme dimension !" << endl << endl;
	}
	Matrice* substraction = new Matrice(sizeMat);
	substraction->fillZero();
	for (unsigned int i = 0; i < sizeMat; ++i) {
		for (unsigned int j = 0; j < sizeMat; ++j) {
			substraction->getMatrice()[i][j] = this->matrice[i][j] - matriceB.matrice[i][j];
		}
	}
	return substraction;
}

// On calibre newSize � la puissance de 2 sup�rieure � size la plus proche
Matrice * Matrice::power2Matrice()
{
	// Nouvelle taille
	unsigned newSize = sizeMat;

	// ---  Optimisation bas�e sur le livre "Hacker�s Delight"  ---
	unsigned i;
	--newSize;

	// On d�cale � gauche bit � bit
	for (i = 1; i < sizeof newSize * CHAR_BIT; i <<= 1) 
	{
		// Masque sur les bits
		// Avec les it�rations, on met � 1 tous les bits � droite du bit � 1 de poids le plus fort
		newSize |= newSize >> i;
	}
	// On ajoute 1 pour obtenir la puissance de 2 correspondante
	++newSize;
	// ------------------------------------------------------------

	// On compl�te la matrice d�origine avec des 0 pour qu�elle soit de taille newSize
	Matrice* newSizeMatrice = new Matrice(newSize);
	newSizeMatrice->fillZero();
	// Copie des valeurs de la matrice d�origine
	for (unsigned int i = 0; i < sizeMat; ++i) {
		for (unsigned int j = 0; j < sizeMat; ++j) {
			newSizeMatrice->getMatrice()[i][j] = matrice[i][j];
		}
	}
	// Ajout des 0 suppl�mentaires
	for (unsigned int i = sizeMat; i < newSize; ++i) {
		for (unsigned int j = sizeMat; j < newSize; ++j) {
			newSizeMatrice->getMatrice()[i][j] = 0;
		}
	}
	// Copie du r�sultat
	*this = *newSizeMatrice;
	return newSizeMatrice;
}

// On d�coupe la matrice en 4 sous-matrice 
void Matrice::splitMatrice(Matrice * matriceUL, Matrice * matriceUR, Matrice * matriceDL, Matrice * matriceDR)
{
	int sizeSplit = this->sizeMat;

	for (unsigned int i = 0; i < sizeSplit; ++i) {
		for (unsigned int j = 0; j < sizeSplit; ++j) {
			// Haut
			if (i < sizeSplit / 2)
			{
				// Haut et gauche
				if (j < sizeSplit / 2)
				{
					matriceUL->getMatrice()[i][j] = this->matrice[i][j];
				}
				// Haut et droite
				else if (j >= (sizeSplit / 2) && j < sizeSplit)
				{
					matriceUR->getMatrice()[i][j - (sizeSplit / 2)] = this->matrice[i][j];
				}
			}
			// Bas
			else if (i >= (sizeSplit / 2) && i < sizeSplit)
			{
				// Bas et gauche
				if (j < sizeSplit / 2)
				{
					matriceDL->getMatrice()[i - (sizeSplit / 2)][j] = this->matrice[i][j];
				}
				// Bas et droite
				else if (j >= (sizeSplit / 2) && j < sizeSplit)
				{
					matriceDR->getMatrice()[i - (sizeSplit / 2)][j - (sizeSplit / 2)] = this->matrice[i][j];
				}
			}
		}
	}
}

void Matrice::rebuildMatrice(Matrice * matriceUL, Matrice * matriceUR, Matrice * matriceDL, Matrice * matriceDR)
{
	// Matrice de r�sultat
	int sizeSplit = (matriceUL->getSizeMat()) * 2;

	for (unsigned int i = 0; i < sizeSplit; ++i) {
		for (unsigned int j = 0; j < sizeSplit; ++j) {
			// Haut
			if (i < sizeSplit / 2)
			{
				// Haut et gauche
				if (j < sizeSplit / 2)
				{
					this->getMatrice()[i][j] = matriceUL->getMatrice()[i][j];
				}
				// Haut et droite
				else if (j >= (sizeSplit / 2) && j < sizeSplit)
				{
					this->getMatrice()[i][j] = matriceUR->getMatrice()[i][j - (sizeSplit / 2)];
				}
			}
			// Bas
			else if (i >= (sizeSplit / 2) && i < sizeSplit)
			{
				// Bas et gauche
				if (j < sizeSplit / 2)
				{
					this->getMatrice()[i][j] = matriceDL->getMatrice()[i - (sizeSplit / 2)][j];
				}
				// Bas et droite
				else if (j >= (sizeSplit / 2) && j < sizeSplit)
				{
					this->getMatrice()[i][j] = matriceDR->getMatrice()[i - (sizeSplit / 2)][j - (sizeSplit / 2)];
				}
			}
		}
	}
}

// On remplit la matrice de 0
void Matrice::fillZero()
{
	// Parcours de chaque case de la matrice
	for (int i = 0; i < sizeMat; i++)
	{
		for (int j = 0; j < sizeMat; j++)
		{
			// On affecte la valeur z�ro et on fais l�affichage
			matrice[i][j] = 0;
		}
	}
}

// On remplit la matrice de nombres al�atoires entre 0 et 9
void Matrice::fillRand()
{
	// Initialisation du random
	//srand(time(NULL));
	for (int i = 0; i < sizeMat; i++)
	{
		for (int j = 0; j < sizeMat; j++)
		{
			matrice[i][j] = rand()%10;
		}
	}
}

// Affichage de la matrice
void Matrice::printMatrice()
{
	for (int i = 0; i < sizeMat; i++)
	{
		cout << endl << "|| ";
		for (int j = 0; j < sizeMat; j++)
		{
			cout << matrice[i][j] << " ";
		}
		cout << " ||";
	}
	cout << endl<<endl;
}

// Retourne la taille de la matrice
int Matrice::getSizeMat() const
{
	return sizeMat;
}

// Retourne les valeurs de la matrice
int ** Matrice::getMatrice() const
{
	return matrice;
}

Matrice* Matrice::classicMultiply(Matrice* matriceA, Matrice* matriceB)
{

	// V�rification de la dimension (on ne travaille qu�avec des matrices carr�s)
	if (matriceA->getSizeMat() != matriceB->getSizeMat())
	{
		cout << "Les matrices n'ont pas la meme dimension !" << endl << endl;
	}
	else
	{
		int countSize = matriceA->getSizeMat();
		// Matrice contenant le r�sultat, initalis� avec uniquement des 0
		Matrice* multiply = new Matrice(countSize);
		multiply->fillZero();
		// Parcours de la matrice
		for (int row = 0; row < countSize; row++) {
			for (int col = 0; col < countSize; col++) {
				for (int offset = 0; offset < countSize; offset++) {
					// Pour une matrice 3x3 : multiply[0][0] = matriceA[0][0]*matriceB[0][0] + matriceA[0][1]*matriceB[1][0] + matriceA[0][2]*matriceB[2][0]
					multiply->getMatrice()[row][col] += matriceA->getMatrice()[row][offset] * matriceB->getMatrice()[offset][col];
				}
			}
		}
		return multiply;
	}

}
