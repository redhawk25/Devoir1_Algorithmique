/********************************************
#       Devoir 1 - Algorithmique            #
#    Nicolas FARINE - UQAC - Et� 2016       #
#                                           #
#     Permet de lire et �crire dans         #
#             un fichier texte              #
#    Bas� sur le projet de cryptographie    #
#        du semestre d'automne 2015         #
#      Nicolas FARINE & Vincent CASTILLE    #
*********************************************/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include "FileHandler.hpp"

using namespace std;


FileHandler::FileHandler()
{

}
//////////////////////////////// Text file //////////////////////////////// 
// reading
string FileHandler::readTextFile(string fileName)
{
	ifstream myFile(fileName, ios::in); // opening file in read mode
	string contenu;
	if (myFile) // verifying that the file is fully opened
	{
		cout << "File opened - reading" << endl;
		string line;
		while (getline(myFile, line)) // getting all lines of the text file
		{
			line += " ";
			contenu += line;
		}
		cout << " File content : "<<endl;
		cout << contenu << endl;	// print the content of the file inside the console
	}
	else //if the file did not opened
		cout << "Error opening file" << endl;
	cout << "Closing File " << endl;
	myFile.close();
	return contenu;		//sending back the content
}

// writing
bool FileHandler::writeTextFile(string fileName, string text)
{
	ofstream myFile(fileName, ios::out); // opening file in write mode
	if (myFile) // verifying that the file is fully opened
	{
		cout << "File opened - writing" << endl;
		myFile << text;		//put the text inside the file
		cout << "Closing File " << endl;
		myFile.close();
		return true;		// operation succeed, sending true boolean
	}
	else	//if the file did not opened
	{
		cout << "Error opening file" << endl;
		return false;	// operation failed, sending false boolean
	}
	return false;
}

FileHandler::~FileHandler()
{

}
