/********************************************
#       Devoir 1 - Algorithmique            #
#    Nicolas FARINE - UQAC - Et� 2016       #
#                                           #
#     Permet de lire et �crire dans         #
#             un fichier texte              #
#    Bas� sur le projet de cryptographie    #
#        du semestre d'automne 2015         #
#      Nicolas FARINE & Vincent CASTILLE    #
*********************************************/

#pragma once
using namespace std;

class FileHandler
{
	public:
		//Constructor
		FileHandler();
		//Text file
		string readTextFile(string fileName);
		bool writeTextFile(string fileName, string text);
		//Destructor
		~FileHandler();

	private:
		unsigned char _info[54]; // used for bmp file to extract the unusefull header
};

